module.exports = {
  db: {
    host: '127.0.0.1',
    port: 27017,
    name: 'justjsblogdemo'
  },
  http: {
    port: 8000
  }
};
