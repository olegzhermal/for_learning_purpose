var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('default', function() {
  gulp.src('./bower_components/foundation-sites/foundation-sites.scss')
      .pipe(sass().on('error', sass.logError))
      .pipe(gulp.dest('./public/stylesheets/'));
  // gulp.watch('./bower_components/foundation-sites/foundation-sites.scss',['default']);


  gulp.src('./sass/*.scss')
      .pipe(sass().on('error', sass.logError))
      .pipe(gulp.dest('./public/stylesheets/'));
  gulp.watch('./sass/*.scss',['default']);
});
