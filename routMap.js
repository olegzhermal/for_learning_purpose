var router = require('koa-router')(),
    dotJS    = require("dot"),
    parse  = require('co-body');

dotJS.templateSettings.strip = false;
dot = dotJS.process({path: "./views"});

var session_data = {};

var ajaxEx = require('./ajaxExample');

router

    .get('/', function* (next) {
        this.body = dot.main(session_data);
    })

    .get('/hello', function* (next) {
        this.body = dot.main(session_data);
    })

    .post('/test', function* (next) {
        var form_data = yield parse.form(this);
        console.log(form_data);
        //full parsed info (use as json object with proprtie access)
        this.body = dot.main_post_test(form_data);
        // TODO understands how I could use mongo.db when mongo does not defined in this module but in parent module
        // Can I do the same for router, dot and parse?
        // mongo defined in index.js
        if (!form_data.post1 == "") {
          this.mongo.db('template').collection('test').insert({'data':form_data.post1});
        } else {
          console.log('field to post was empty');
        }
    })

    .post('/ajaxUrl', function* (next) {
        switch(this.request.header.ajaxroute) {
            case "English":
            this.body = ajaxEx.sayHelloInEnglish();
            break;
            case "Spanish":
            this.body = ajaxEx.sayHelloInSpanish();
            break;
            default:
            this.body = ajaxEx.sayHelloInEnglish();
        }
    });

module.exports = router.middleware();
