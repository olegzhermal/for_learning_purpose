'use strict';

require('dotenv').config();

var app    = require('koa')(),
    serve  = require('koa-static'),
    assert = require('assert'),
    scss   = require('koa-scss'),
    mongo  = require('koa-mongo'),
    mount  = require('koa-mount'),
    helmet = require('koa-helmet');

// defining port to listen
var port = process.env.PORT ? process.env.PORT : 8000;

if (process.env.USING_DB && process.env.LOGNAME == 'olegzhermal') {app.use(mongo())}
else if (process.env.USING_DB) {
  // getting db working with heroku
  app.use(mongo({
      host: 'ds015939.mlab.com',
      port: 15939,
      user: 'herokuoz',
      pass: 'sophia14',
      db: 'heroku_xkx0tp40'
    }));
    process.env.DB_NAME = 'heroku_xkx0tp40';
    process.env.DB_COLLECTION = 'template';
}
else {console.log("In order to get DB functionality uncomment DB field in .env")}

app
    .use(helmet())
    // .use(mongo())
    .use(mount('/', require('./routMap.js')))
    .use(scss({
        src: __dirname + '/sass',
        dest: __dirname + '/public/stylesheets'
    }))

    // dirs where static files are served from
      .use(serve(__dirname + '/public/stylesheets'))
      .use(serve(__dirname + '/public/javascript'))

    // using koa-router middleware for mapped routes
    // .use(router.routes())
    // .use(router.allowedMethods())
    // instead of 3 lines above koa-mount is used to mount koa-router

    // bellow is for all the routes not used by koa-static or koa-router
    .use(function *() {
      this.body = 'Invalid URL!!!';
      console.log(this.request.url + " NOT FOUND");
      // or another callback
    });

if (!module.parent) {
  app.listen(port);
  console.log('Koa is listening on port ' + port);
}
